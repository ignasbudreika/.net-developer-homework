﻿using System;
using System.Collections.Generic;
using static ContactManager.Globals;

namespace ContactManager
{
    class Add
    {
        public static void AddScreen()
        {
            Console.Clear();
            Console.WriteLine(HEADER + "\n");
            Console.WriteLine("Add new contact\n");

            Contact newContact = In.ReadContactInfo();

            if (!InformationIsValid(newContact))
            {
                Console.Write("Press any key to return to main menu... ");
                Console.ReadKey();
                return;
            }

            if (PhoneNumberAvailable(newContact))
            {
                Out.PrintNewContact(newContact);
                Console.WriteLine("\nContact has been added");
            }
            else
            {
                Console.WriteLine("\nPhone number is already taken");
            }

            Console.Write("Press any key to return to main menu... ");
            Console.ReadKey();
        }
        public static bool PhoneNumberAvailable(Contact newContact)
        {
            List<Contact> allContacts = In.ReadAllContacts();

            foreach (Contact contact in allContacts)
                if (newContact.PhoneNumber.Equals(contact.PhoneNumber))
                    return false;

            return true;
        }
        public static bool InformationIsValid(Contact newContact)
        {
            if (newContact.Name.Equals(string.Empty))
            {
                Console.WriteLine("\nName cannot be empty");
                return false;
            }
            else if (newContact.LastName.Equals(string.Empty))
            {
                Console.WriteLine("\nLast name cannot be empty");
                return false;
            }
            else if (newContact.PhoneNumber.Equals(string.Empty))
            {
                Console.WriteLine("\nPhone number cannot be empty");
                return false;
            }
            return true;
        }
    }
}
