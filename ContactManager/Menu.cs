﻿using System;
using System.Text;
using static ContactManager.Globals;

namespace ContactManager
{
    class Menu
    {
        private static readonly string[] MenuOptions = { "Add contact", "Update contact", "Delete contact", "View all contacts" };
        public static bool MenuScreen()
        {
            Console.Clear();

            Console.WriteLine(HEADER + "\n");

            for (int i = 0; i < MenuOptions.Length; i++)
            {
                StringBuilder line = new StringBuilder();

                line.Append(i + 1);
                line.Append(" " + MenuOptions[i]);

                Console.WriteLine(line);
            }

            Console.Write("\nSelect an action (1-4): ");
            var input = Console.ReadLine();

            switch (input)
            {
                case "1":
                    Add.AddScreen();
                    return true;
                case "2":
                    Update.UpdateScreen();
                    return true;
                case "3":
                    Delete.DeleteScreen();
                    return true;
                case "4":
                    ViewAll.ViewAllScreen();
                    return true;
                default:
                    Console.Write("Are you sure you want to exit? Y/N: ");
                    var key = Console.ReadKey().KeyChar;
                    if (key.Equals('y') || key.Equals('Y'))
                        return false;
                    return true;
            }
        }
    }
}