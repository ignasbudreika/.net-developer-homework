﻿namespace ContactManager
{
    class Contact
    {
        public Contact(string name, string lastName, string phoneNumber, string address)
        {
            Name = name;
            LastName = lastName;
            PhoneNumber = phoneNumber;
            Address = address;
        }
        public Contact(string name, string lastName, string phoneNumber)
        {
            Name = name;
            LastName = lastName;
            PhoneNumber = phoneNumber;
        }

        public string Name { get; private set; }
        public string LastName { get; private set; }
        public string PhoneNumber { get; private set; }
        public string Address { get; private set; }

        public override string ToString()
        {
            return string.Format("{0};{1};{2};{3}", Name, LastName, PhoneNumber, Address);
        }
        public string ToStringTable()
        {
            return string.Format("| {0, -15} | {1, -20} | {2, 15} | {3, -20} |", Name, LastName, PhoneNumber, Address);
        }
    }
}
