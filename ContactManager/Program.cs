﻿namespace ContactManager
{
    class Program
    {
        static void Main(string[] args)
        {
            bool proceed = true;

            while (proceed)
            {
                proceed = Menu.MenuScreen();
            }
        }
    }
}
