﻿using System.Collections.Generic;
using System.IO;
using static ContactManager.Globals;

namespace ContactManager
{
    class Out
    {
        public static void PrintNewContact(Contact newContact)
        {
            using(StreamWriter writer = File.AppendText(PATH))
                writer.WriteLine(newContact.ToString());
        }
        public static void PrintAllContacts(List<Contact> allContacts)
        {
            string[] lines = new string[allContacts.Count];

            for (int i = 0; i < allContacts.Count; i++)
                lines[i] = allContacts[i].ToString();

            File.WriteAllLines(PATH, lines);
        }
    }
}
