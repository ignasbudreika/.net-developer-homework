﻿using System;
using System.Collections.Generic;
using static ContactManager.Globals;

namespace ContactManager
{
    class Update
    {
        public static void UpdateScreen()
        {
            Console.Clear();
            Console.WriteLine(HEADER + "\n");
            Console.WriteLine("Update contact\n");

            Console.Write("Write phone number of a contact that you want to update: ");
            string phoneNumber = Console.ReadLine();

            List<Contact> allContacts = In.ReadAllContacts();
            bool exists = false;

            for (int i = 0; i < allContacts.Count; i++)
            {
                if (allContacts[i].PhoneNumber.Equals(phoneNumber))
                {
                    Console.WriteLine(new string('-', 83));
                    Console.WriteLine("| {0, -15} | {1, -20} | {2, -15} | {3, -20} |", "Name", "Last name", "Phone number", "Address");
                    Console.WriteLine(new string('-', 83));
                    Console.WriteLine(allContacts[i].ToStringTable());
                    Console.WriteLine(new string('-', 83));
                    Console.WriteLine();
                    exists = true;
                }
            }

            if (!exists)
            {
                Console.WriteLine("\nContact with such phone number does not exist");
                Console.Write("Press any key to return to main menu... ");
                Console.ReadKey();
                return;
            }
            else
            {
                Contact newContact = In.ReadContactInfo();

                if (!Add.InformationIsValid(newContact))
                {
                    Console.Write("Press any key to return to main menu... ");
                    Console.ReadKey();
                    return;
                }

                if (PhoneNumberAvailable(newContact, phoneNumber))
                {
                    for (int i = 0; i < allContacts.Count; i++)
                    {
                        if (allContacts[i].PhoneNumber.Equals(phoneNumber))
                        {
                            allContacts[i] = newContact;
                            break;
                        }
                    }
                    Out.PrintAllContacts(allContacts);
                    Console.WriteLine("\nContact has been updated");
                    Console.WriteLine(new string('-', 83));
                    Console.WriteLine("| {0, -15} | {1, -20} | {2, -15} | {3, -20} |", "Name", "Last name", "Phone number", "Address");
                    Console.WriteLine(new string('-', 83));
                    Console.WriteLine(newContact.ToStringTable());
                    Console.WriteLine(new string('-', 83));
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("\nPhone number is already taken");
                }

                Console.Write("Press any key to return to main menu... ");
                Console.ReadKey();
            }
        }
        public static bool PhoneNumberAvailable(Contact newContact, string phoneNumber)
        {
            List<Contact> allContacts = In.ReadAllContacts();

            foreach (Contact contact in allContacts)
                if (newContact.PhoneNumber.Equals(contact.PhoneNumber) && contact.PhoneNumber != phoneNumber)
                    return false;

            return true;
        }
    }
}
