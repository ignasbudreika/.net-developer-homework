﻿using System;
using System.Collections.Generic;
using System.IO;
using static ContactManager.Globals;

namespace ContactManager
{
    class In
    {
        public static List<Contact> ReadAllContacts()
        {
            List<Contact> contacts = new List<Contact>();

            if (File.Exists(PATH))
            {
                using (StreamReader reader = new StreamReader(PATH))
                {
                    string line;

                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] parts = line.Split(';');

                        if (parts.Length == 4)
                        {
                            Contact contact = new Contact(parts[0], parts[1], parts[2], parts[3]);
                            contacts.Add(contact);
                        }
                        if (parts.Length == 3)
                        {
                            Contact contact = new Contact(parts[0], parts[1], parts[2]);
                            contacts.Add(contact);
                        }
                    }
                }
            }

            return contacts;
        }
        public static Contact ReadContactInfo()
        {
            Console.Write("Name: ");
            string name = Console.ReadLine();

            Console.Write("Last name: ");
            string lastName = Console.ReadLine();

            Console.Write("Phone number: ");
            string phoneNumber = Console.ReadLine();

            Console.Write("Address (optional): ");
            string address = Console.ReadLine();

            if (address.Equals(string.Empty))
                return new Contact(name, lastName, phoneNumber);
            else
                return new Contact(name, lastName, phoneNumber, address);
        }
        public static string ReadPhoneNumberToDelete()
        {
            Console.Write("Write phone number to delete: ");
            string phoneNumber = Console.ReadLine();

            return phoneNumber;
        }
    }
}
