﻿namespace ContactManager
{
    public static class Globals
    {
        /// <summary>
        /// Application's header
        /// </summary>
        public const string HEADER = "CONTACT MANAGER";
        /// <summary>
        /// Contacts' data file
        /// </summary>
        public const string PATH = "Contacts.csv";
    }
}
