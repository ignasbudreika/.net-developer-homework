﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static ContactManager.Globals;

namespace ContactManager
{
    class ViewAll
    {
        public static void ViewAllScreen()
        {
            List<Contact> allContacts = In.ReadAllContacts();

            Console.Clear();
            Console.WriteLine(HEADER + "\n");
            Console.WriteLine("All contacts\n");

            if(allContacts.Count > 0)
            {
                Console.WriteLine(new string('-', 83));
                Console.WriteLine("| {0, -15} | {1, -20} | {2, -15} | {3, -20} |", "Name", "Last name", "Phone number", "Address");
                Console.WriteLine(new string('-', 83));

                foreach (Contact contact in allContacts)
                {
                    Console.WriteLine(contact.ToStringTable());
                    Console.WriteLine(new string('-', 83));
                }
            }
            else
            {
                Console.WriteLine("Contact list is empty");
            }

            Console.Write("\nPress any key to return to main menu... ");
            Console.ReadKey();
        }
    }
}
