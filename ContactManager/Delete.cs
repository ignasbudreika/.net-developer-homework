﻿using System;
using System.Collections.Generic;
using static ContactManager.Globals;

namespace ContactManager
{
    class Delete
    {
        public static void DeleteScreen()
        {
            Console.Clear();
            Console.WriteLine(HEADER + "\n");
            Console.WriteLine("Delete contact\n");

            string phoneNumber = In.ReadPhoneNumberToDelete();

            if (phoneNumber.Equals(string.Empty))
            {
                Console.WriteLine("\nPhone number cannot be empty ");
                Console.Write("Press any key to return to main menu... ");
                Console.ReadKey();
                return;
            }

            List<Contact> allContacts = In.ReadAllContacts();
            bool removed = false;

            for(int i = 0; i < allContacts.Count; i++)
            {
                if (allContacts[i].PhoneNumber.Equals(phoneNumber))
                {
                    removed = true;
                    allContacts.RemoveAt(i);
                    break;
                }
            }

            if (removed)
            {
                Console.WriteLine("\nContact has been successfully deleted");
                Out.PrintAllContacts(allContacts);
            }
            else
            {
                Console.WriteLine("\nContact with such phone number does not exist");
            }

            Console.Write("Press any key to return to main menu... ");
            Console.ReadKey();
        }
    }
}
